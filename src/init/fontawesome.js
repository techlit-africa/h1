import {library, config} from '@fortawesome/fontawesome-svg-core'

import {
  faChevronLeft,
  faChevronRight,
  faArrowAltCircleRight,
  faArrowAltCircleLeft,
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faChevronLeft,
  faChevronRight,
  faArrowAltCircleRight,
  faArrowAltCircleLeft,
)

config.autoAddCss = false
