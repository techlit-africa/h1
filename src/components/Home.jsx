import {Editor} from 'components/Editor'

export const Home = () => {
  const [code, setCode] = React.useState('')

  return pug`
    .w-full.h-screen.flex.flex-row
      .w-full.max-w-prose.h-full.flex.flex-col
        .w-full.flex.flex-col(className='h-1/2')
          .w-full.flex-1.bg-black
          .w-full.h-10.flex.flex-row.items-center.justify-between.bg-gray-800.border-t.border-b.border-gray-500
            button.flex.items-center.px-2.text-blue-300
              Icon.text-xl(icon='chevron-left')
              span.pl-2 Back
            button.flex.items-center.px-2.text-blue-300
              span.pr-2 Next
              Icon.text-xl(icon='chevron-right')

        .w-full.flex.flex-col(className='h-1/2')
          .w-full.flex-1.overflow-hidden
            Editor.w-full.h-full(
              code=code
              setCode=setCode
            )

      iframe.flex-1.h-full(srcdoc=code)
  `
}
