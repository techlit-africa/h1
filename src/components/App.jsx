import {Switch, Route, Redirect} from 'react-router-dom'
import {Home} from 'components/Home'
// import {Exercise} from 'components/Exercise'
// import {Submission} from 'components/Submission'

export const App = () => {
  return pug`
    Switch
      Route(path='/home') #[Home]
      // Route(path='/exercises/:cuid') #[Exercise]
      // Route(path='/submissions/:cuid') #[Submission]
      Route #[Redirect(to='/home')]
  `
}
