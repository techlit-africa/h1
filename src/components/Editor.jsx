import {Controlled as CodeMirror} from 'react-codemirror2'

import 'codemirror/mode/xml/xml'
import 'codemirror/mode/css/css'
import 'codemirror/mode/javascript/javascript'
import 'codemirror/mode/htmlmixed/htmlmixed'

import 'codemirror/lib/codemirror.css'
import 'codemirror/theme/material.css'

export const Editor = ({code, setCode, className=''}) => pug`
  CodeMirror(
    className=className
    value=code
    onBeforeChange=(e,d,v)=>setCode(v)
    options={
      mode: 'htmlmixed',
      theme: 'material',
      lineNumbers: true,
    }
  )
`
