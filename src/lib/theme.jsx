export const LIGHTS_ON_KEY = 'techlit.lightsOn'

export const getLightsOn = () => window.localStorage.getItem(LIGHTS_ON_KEY) !== 'false'
export const setLightsOn = (lightsOn) => {
  window.localStorage.setItem(LIGHTS_ON_KEY, lightsOn)
  if (lightsOn) {
    document.body.classList.remove('dark')
  } else {
    document.body.classList.add('dark')
  }
}
export const syncLightsOn = () => setLightsOn(getLightsOn())
export const toggleLightsOn = () => setLightsOn(!getLightsOn())
